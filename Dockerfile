FROM node:10.15
MAINTAINER williewu

RUN npm config set registry https://registry.npm.taobao.org

ENV HOST 0.0.0.0
ENV NODE_ENV=docker

WORKDIR /app

COPY . .
RUN npm install

RUN npm run build

